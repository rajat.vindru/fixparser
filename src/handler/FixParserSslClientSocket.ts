import { EventEmitter } from 'events';
import FIXParser from '../FIXParser';
import Message from '../message/Message';
import FIXParserClientBase from './FIXParserClientBase';
import tls from 'tls';

export default class FIXParserSslClientSocket extends FIXParserClientBase {
    private connected: boolean = false;
    private socket: any;
    constructor(eventEmitter: EventEmitter, parser: FIXParser) {
        super(eventEmitter, parser);
    }

    public connect() {
        const options = {
            key: this.sslKey!,
            cert: this.sslCert!,
            rejectUnauthorized: false,
        };

        this.socket = tls.connect(this.port!, this.host!, options, () => {
            this.connected = true;
            this.eventEmitter!.emit('open');
            this.startHeartbeat();
            console.log('Connected through SSL');

            process.stdin.pipe(this.socket);
            process.stdin.resume();
        });
        this.socket!.setEncoding('utf8');
        this.socket!.on('data', (data: any) => {
            console.log(data);
            this.eventEmitter!.emit('message', data);
        });

        this.socket!.on('error', (error: any) => {
            this.connected = false;
            this.eventEmitter!.emit('error', error);
            this.stopHeartbeat();
        });

        this.socket!.on('close', () => {
            this.connected = false;
            this.eventEmitter!.emit('close');
            this.stopHeartbeat();
        });

        this.socket!.on('timeout', () => {
            this.connected = false;
            this.eventEmitter!.emit('timeout');
            this.socketTCP!.end();
            this.stopHeartbeat();
        });
    }

    public close() {
        if (this.socketTCP) {
            this.socketTCP!.destroy();
        } else {
            console.error(
                'FIXParserSslClientSocket: could not close socket, connection not open',
            );
        }
    }

    public send(message: Message) {
        if (this.connected) {
            this.fixParser!.setNextTargetMsgSeqNum(
                this.fixParser!.getNextTargetMsgSeqNum() + 1,
            );
            this.socket.write(message.encode());
        } else {
            console.error(
                'FIXParserSslClientSocket: could not send message, socket not open',
                message,
            );
        }
    }
}
